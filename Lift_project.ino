#include <ContinuousServo.h>

const int servoSize = 9;
ContinuousServo *myServos[9];  // create servo object to control a servo

//up speed for each servo (port start at 2)
int servoUpSpeeds[9] = { -20, 11/3, 2, 9/2, 8/2, 8/2, 31/2, 8/2, 10/2};
//down speed for each servo (port start at 2)
int servoDownSpeed[9] = { 28, -45/3, -45/4, -45/3, -45/3, -45/3, -45/3, -45/3, -45/3};

//data for save servo step
//0:up 1:up 2:down
int servoNowStep[9];

//pin start at 22
int p1ButtonPins[9];
int p1ButtonStates[9];
//pin start at 32
int p2ButtonPins[9];
int p2ButtonStates[9];

bool isServosMoving = false;

void setup() {
  Serial.begin(9600);
  Serial.println("<Arduino is ready>");

  for(int i = 0; i < servoSize; i++){
    myServos[i] = new ContinuousServo( i + 2 );

    p1ButtonPins[i] = i + 22;
    // initialize the pushbutton pin as an input:
    pinMode(p1ButtonPins[i], INPUT_PULLUP);

    p2ButtonPins[i] = i + 32;
    // initialize the pushbutton pin as an input:
    pinMode(p2ButtonPins[i], INPUT_PULLUP);
  }
}

void loop() { 
  buttonController();
  //keyboard input  test for serial monitor
  inputController(); 
}

void buttonController(){
  //return if reset not finished
  if(isServosMoving) return;

  //p1 button controller
  for(int i = 0; i < servoSize; i++)
  {
    p1ButtonStates[i] = digitalRead(p1ButtonPins[i]);
    int btnState = p1ButtonStates[i];
    if (btnState == LOW) {
      //Serial.print("btn: ");Serial.print(btnState);Serial.print('\n');
      setServoRot(i);  
    }
  }

  //p2 button controller
  for(int i = 0; i < servoSize; i++)
  {
    p2ButtonStates[i] = digitalRead(p2ButtonPins[i]);
    int btnState = p2ButtonStates[i];
    if (btnState == LOW) {
      //Serial.print("btn: ");Serial.print(btnState);Serial.print('\n');
      setServoRot(i);  
    }
  }
}

//arduino test
char input; 
void inputController(){
  //return if reset not finished
  if(Serial.available() <= 0 || isServosMoving) return;
    
  input = Serial.read();

  if(isDigit(input)){
    setServoRot(input - '0');  
  }
  else if(input == 'd'){
    resetServos();
  }
  else if(input == 's'){
    moveServos();
  }
}

//reset all servos to postion initiate
void resetServos(){

  for(int i =0; i < servoSize; i++){
    ContinuousServo *servo = myServos[i];
    if(servoNowStep[i] >0){
      if(!isServosMoving) isServosMoving = true;
      servo ->step(servoDownSpeed[i] * servoNowStep[i], resetFinished);
      
      servoNowStep[i] = 0;
      //use this delay if your want a delay between two servos
      //delay(10); 
    }
  } 
}

//move all servos to next step
void moveServos(){
  for(int i =0; i < servoSize; i++){
    setServoRot(i);
    //use this delay if your want a delay between two servos
    //delay(10); 
  } 
}

//check if all servos are finished the move
void resetFinished(){
  //return if reset not finished
  for(int i = 0; i < servoSize; i ++){
    if(myServos[i] -> isActive()) return;  
  }
  isServosMoving = false;
}

//set one servo move to next step
void setServoRot(int index){
  //return if reset not finished
  //return if out of range
  if(index > servoSize -1)return;

  ContinuousServo *servo = myServos[index];
  if(servo -> isActive()) return;
  
  int moveStep;
  if(servoNowStep[index] < 2){
    servoNowStep[index] +=1;
    //calculate move step
    moveStep = servoUpSpeeds[index];
  }
  //servo reset down
  else{
    moveStep = servoDownSpeed[index] * servoNowStep[index];
    servoNowStep[index] =0;
  }

//block player controller
  isServosMoving = true;
  servo ->step(moveStep , resetFinished);
  
  Serial.print("index is: ");Serial.print(index);
  Serial.print("  start: ");Serial.print(moveStep);Serial.print('\n');
}
